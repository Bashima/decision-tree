import java.util.ArrayList;

public class Entropy {
	public ArrayList<int[]> data;
	public int attribute;
	public double entropy;
	public int columnCount;
	public int rowCount;
	public ArrayList<Typ_Def> types;

	public Entropy(ArrayList<int[]> a, int ab, int col, int row) {
		data = new ArrayList<int[]>();
		@SuppressWarnings("unchecked")
		ArrayList<int[]> clone = (ArrayList<int[]>) a.clone();
		data = clone;
		attribute = ab;
		types = new ArrayList<Typ_Def>();
		columnCount = col;
		rowCount = row;
		entropy = 0;
	}

	public void calculateEntropy(double[] w) {
		double weightsum = 0.0;
		for (int i = 0; i < w.length; i++) {
			weightsum += w[i];
		}
		if (attribute == -1) {
			double plus = 0, minus = 0;
			int c1 =0;
			for (int[] a : data) {
				if (a[columnCount - 1] == 0)
					minus= minus + w[c1];
				else
					plus = plus + w[c1];
				c1++;
			}

			
			double pPlus = (plus * 1.0) / (weightsum * 1.0);
			double pMinus = (minus * 1.0) / (weightsum * 1.0);
			if (rowCount == 0 || plus == 0 || minus == 0) {
				entropy = 0;
			} else {
				entropy = -1.0
						* (pPlus * (Math.log(pPlus) / Math.log(2)) + (pMinus)
								* (Math.log(pMinus) / Math.log(2)));
//				System.out.println(entropy);
			}

		} else {
			ArrayList<Integer> tpes = new ArrayList<Integer>();
			int c2 =0;
			for (int[] n : data) {
				if (tpes.contains(n[attribute])) {
					for (Typ_Def t : types) {
						if (t.type == n[attribute]) {
							if (n[columnCount - 1] == 0)
								t.minus= t.minus + w[c2];
							else if (n[columnCount - 1] == 1)
								t.plus = t.plus + w[c2];
							// System.out.println(t.type +" "+ t.minus+ " "+
							// t.plus);
						}
					}
					// do not add
				} else {
					tpes.add(n[attribute]);
					Typ_Def temp = new Typ_Def();
					temp.type = n[attribute];
					if (n[columnCount - 1] == 0)
						temp.minus = temp.minus + w[c2];
					else if (n[columnCount - 1] == 1)
						temp.plus = temp.plus + w[c2];
					types.add(temp);

				}
				c2++;
			}
			// System.out.println(types.toString());

			for (Typ_Def tp : types) {
				double total = tp.plus + tp.minus;
//				System.out.println(total + " " + tp.plus + " " + tp.minus);
				double pPlus = (tp.plus * 1.0) / (total * 1.0);
				double pMinus = (tp.minus * 1.0) / (total * 1.0);
				if (total == 0 || tp.plus == 0 || tp.minus == 0) {
					tp.entro = 0;
				} else {
					tp.entro = -1.0
							* (pPlus * (Math.log(pPlus) / Math.log(2)) + (pMinus)
									* (Math.log(pMinus) / Math.log(2)));
//					System.out.println(tp.entro);
				}

				entropy += ((total * 1.0) / (weightsum * 1.0) * tp.entro);
//				System.out.println(entropy);
			}
		}
	}

}
