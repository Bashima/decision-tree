import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Read_CSV {

	public ArrayList<int[]> rows;
	public int rowCount;
	public int columnCount;
	
	Read_CSV(){
		rows = new ArrayList<int[]>();
		rowCount = 0;
	}
	
	public ArrayList<int[]> read(String filename)
	{

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(filename));
			while ((line = br.readLine()) != null) {
//				System.out.println(line);
				String[] columnString = line.split(cvsSplitBy);
				columnCount = columnString.length;
				int [] column = new int[columnCount];
//				System.out.println(columnCount);
				for(int i=0; i<columnString.length; i++)
				{
					column[i] = Integer.parseInt(columnString[i]);
				}
				rows.add(column);
				rowCount++;
			}

		} catch (FileNotFoundException e) {
			System.out.println("No file found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("IO Exception");
					e.printStackTrace();
				}
			}
		}
	  return rows;
	}
	
	public void print()
	{
		System.out.println(rows.toString());
	}
}
