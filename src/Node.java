import java.util.ArrayList;


public class Node {
	public int attribute;
	public int parentAttribute;
	public ArrayList<Node> children=new ArrayList<Node>();
	public int yes[] = new int[11];
	public int no[] = new int[11];
	public double entropy;
	@Override
	public String toString() {
		return "Node [attribute=" + attribute + ", entropy=" + entropy + "]";
	}
	public void printYes()
	{
		for(int i=0; i< 11; i++)
		{
			System.out.println(yes[i]);
		}
	}
	public void printNo()
	{
		for(int i=0; i< 11; i++)
		{
			System.out.println(no[i]);
		}
	}
	
	
}
