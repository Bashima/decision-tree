import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Decision_Tree {
	public ArrayList<int[]> data;
	public ArrayList<int[]> testData;
	public ArrayList<Integer> ar;
	public double[] weight;
	public double[] p_r;
	public int column;
	public int row;
	public int zeroCount;
	public int oneCount;
	int truePositive;
	int falsePositive;
	int trueNegative;
	int falseNegative;
	double precision;
	double fMeasure;
	double recall;
	double accuracy;
	double gMean;
	int invalid;
	public ArrayList<Node> roots;
	public ArrayList<Double> alphas;
	public int itteration;

	public Decision_Tree() {
		data = new ArrayList<int[]>();
		testData = new ArrayList<int[]>();
		ar = new ArrayList<Integer>();
		roots = new ArrayList<Node>();
		alphas = new ArrayList<Double>();
		row = 0;
		column = 0;
		zeroCount = 0;
		oneCount = 0;
		trueNegative = 0;
		truePositive = 0;
		falseNegative = 0;
		falsePositive = 0;
		precision = 0.0;
		fMeasure = 0.0;
		recall = 0.0;
		accuracy = 0.0;
		gMean = 0.0;
		invalid = retA();
		itteration = 5;
	}

	public int retA() {
		Random r = new Random();
		int i = r.nextInt() % 2;
		return i;
	}

	public int calculateBestAttribute(ArrayList<int[]> data1, int column,
			int row, ArrayList<Integer> attributes, double[] w) {
		Entropy eParent = new Entropy(data1, -1, column, row);
		eParent.calculateEntropy(w);
		int best = 0;
		double max = -1000;
		for (int col = 0; col < column - 1; col++) {
			if (attributes.contains(col)) {
			} else {
				Entropy e = new Entropy(data1, col, column, row);
				e.calculateEntropy(w);
				double gain = eParent.entropy - e.entropy;
				if (gain > max) {
					max = gain;
					best = col;
				}
			}
		}
		// System.out.println("best"+best);
		return best;

	}

	public Node ID3(double[] w) {
		Node root = new Node();
		ArrayList<Integer> attributes = new ArrayList<Integer>();

		root.attribute = calculateBestAttribute(data, column, row, attributes,
				w);
		// root.parentAttribute = ;
		Entropy e = new Entropy(data, root.attribute, column, row);
		e.calculateEntropy(w);
		root.entropy = e.entropy;
		// System.out.println(root.attribute + " "+ root.entropy);
		attributes.add(root.attribute);
		// ID3_Reccursion(data, root, attributes);
		System.out.println(root.toString());
		root = countYes(root);
		// root.printYes();
		return root;
	}

	public Node countYes(Node node) {
		for (int i = 0; i < 11; i++) {
			node.yes[i] = 0;
		}
		for (int i = 0; i < 11; i++) {
			node.no[i] = 0;
		}
		for (int[] d : data) {
			// System.out.println(d[node.attribute]);
			if (d[d.length - 1] == 1) {
				node.yes[d[node.attribute]] += 1;
			} else {
				node.no[d[node.attribute]] += 1;
			}
		}
		return node;

	}

	// public void ID3_Reccursion(ArrayList<int[]> data, Node parent,
	// ArrayList<Integer> attributes) {
	// int plus = 0;
	// int minus = 0;
	// Node child1 = new Node();
	// for (int[] n : data) {
	// if (n[column - 1] == 1) {
	// plus++;
	// } else {
	// minus++;
	// }
	//
	// }
	// if (plus == data.size()) {
	// child1.attribute = -10;
	// child1.parentAttribute = parent.attribute;
	// } else if (minus == data.size()) {
	// child1.attribute = -20;
	// child1.parentAttribute = parent.attribute;
	// } else if (data.size() == 0) {
	// if (zeroCount > oneCount) {
	// child1.attribute = -20;
	// child1.parentAttribute = parent.attribute;
	// } else if (zeroCount < oneCount) {
	// child1.attribute = -10;
	// child1.parentAttribute = parent.attribute;
	// }
	// } else {
	// ArrayList<ArrayList<int[]>> dataSet;
	// dataSet = new ArrayList<ArrayList<int[]>>();
	// for (int val = 1; val < 11; val++) {
	//
	// ArrayList<int[]> dataChild = new ArrayList<int[]>();
	// for (int[] n : data) {
	// // System.out.println(parent.attribute);
	// if (parent.attribute == -1) {
	// } else {
	// if (n[parent.attribute] == val) {
	// dataChild.add(n);
	// }
	// }
	// }
	public double initial = 35.0;
	// dataSet.add(dataChild);
	// }
	// int k = 1;
	// for (ArrayList<int[]> arrayList : dataSet) {
	// Node child = new Node();
	// child.parentAttribute = k++;
	// if (plus == data.size()) {
	// child.attribute = -10;
	// parent.children.add(child);
	// } else if (minus == data.size()) {
	// child.attribute = -20;
	// parent.children.add(child);
	// } else if (data.size() == 0) {
	// if (zeroCount > oneCount) {
	// child.attribute = -20;
	// parent.children.add(child);
	// } else if (zeroCount < oneCount) {
	// child.attribute = -10;
	// parent.children.add(child);
	// }
	// } else {
	// int r = arrayList.size();
	// child.attribute = calculateBestAttribute(arrayList, column,
	// r, attributes);
	// Entropy e = new Entropy(arrayList, child.attribute, column,
	// r);
	// e.calculateEntropy();
	// child.entropy = e.entropy;
	//
	// // if(attributes.contains(child.attribute))
	// // {}else{
	// // // attributes.add(child.attribute);
	// // }
	// ID3_Reccursion(arrayList, child, attributes);
	// parent.children.add(child);
	// }
	// // parent.children.add(child);
	// }
	// }
	// }

	public int Traversal(int[] r, Node n) {
		int childSize = n.children.size();
		if (childSize == 0) {
			switch (n.attribute) {
			case -10:
				return 1;
			case -20:
				return 0;
			}
		} else if (n.children.get(0).attribute == -10) {
			return 1;
		} else if (n.children.get(0).attribute == -20) {
			return 0;
		} else {
			for (Node child : n.children) {
				int value = r[n.attribute];
				int temp = -50;
				if (child.parentAttribute == value) {
					temp = Traversal(r, child);
					return temp;
				}
			}
		}
		return 1;
	}

	public int checkVal(int[] r, Node n) {
		int attr = r[n.attribute];
		// System.err.println(attr);
		if (n.yes[attr] > n.no[attr]) {
			// System.err.println("1");
			return 1;
		} else {
			// System.err.println("0");
			return 0;
		}
	}

	public double getWSum() {
		double weightsum = 0.0;
		for (int i = 0; i < row; i++) {
			weightsum += weight[i];
		}
		return weightsum;
	}

	public void ADABoost() {
		for (int i = 0; i < row; i++) {
			weight[i] = 1.0 / (row * 1.0);
		}

		for (int r = 0; r < itteration; r++) {
			double weightsum = getWSum();
			for (int i = 0; i < row; i++) {
				p_r[i] = weight[i] * 1.0 / weightsum;
				// System.out.println("pr; "+weightsum);
			}
			Node root = new Node();
			root = ID3(p_r);
			roots.add(root);
			double error = 0.0;
			int c = 0;
			for (int[] is : data) {
				int ret = checkVal(is, root);
				if (ret != is[9]) {
					error = error + p_r[c];
					if(error>0.5)
						return;
				}
				c++;
			}
			double alpha = 0.0;
			alpha = (0.5) * Math.log((1 - error) / error);
			alphas.add(alpha);
			// System.out.println("alpha: "+alpha+ " error: "+ error);
			for (int i = 0; i < row; i++) {
				// System.out.println(i +" "+ data.size()+ " "+ row);
				int Y = -1;
				int ret = checkVal(data.get(i), root);
				if (ret == data.get(i)[9]) {
					Y = 1;
				}
				weight[i] = weight[i] * Math.exp(-1*alpha * Y);
				// System.err.println(weight[i]);
			}
		}
	}

	public void test(int k) {
		double one = 0.0, zero = 0.0;
		double accuracy =initial;
//		System.out.println("ro: "+ roots.size());
		for (int[] tes : testData) {
			int classi =0;
			for (int i = 0; i < roots.size(); i++) {
				Node classifier = roots.get(i);
				int ret = checkVal(tes, classifier);
				// System.out.println("tes: "+tes+" " +ret);
				if (ret == 0) {
					zero = zero + weight[i];
				} else {
					one = one + weight[i];
				}
			}
			if(zero>one)
			{
				classi =0;
			}
			else
			{
				classi = 1;
			}
			if(classi == tes[9])
			{
				accuracy +=1;
			}
		}
		
		accuracy = (testData.size()- accuracy)/testData.size();
		System.out.println((1-accuracy)*100);
	}

	public static void main(String[] args) {
		Decision_Tree dt = new Decision_Tree();
		Read_CSV r = new Read_CSV();
		ArrayList<int[]> d = new ArrayList<int[]>();
		d = r.read("data.csv");
		// System.out.println(d.get(0)[0]);
		for (int i = 0; i < d.size(); i++) {
			dt.ar.add(i);
		}
		Collections.shuffle(dt.ar);
		for (int i = 0; i < d.size() * .8; i++) {
			// for (int i = 0; i < d.size(); i++) {
			int ind = dt.ar.get(i);
			dt.data.add(d.get(ind));
		}
		for (int j = (int) Math.ceil(d.size() * .8); j < d.size(); j++) {
			int ind = dt.ar.get(j);
			dt.testData.add(d.get(ind));
		}
		dt.row = dt.data.size();
		dt.weight = new double[dt.row];
		dt.p_r = new double[dt.row];
		dt.column = r.columnCount;
		for (int[] n : dt.data) {
			if (n[dt.column - 1] == 0) {
				dt.zeroCount++;
			} else {
				dt.oneCount++;
			}
		}
		dt.itteration = 30;
		dt.ADABoost();
		dt.test(dt.itteration);
//		for (int[] is : dt.testData) {
//			// int ret = dt.Traversal(is, root);
//			int ret = dt.checkVal(is, root);
//			if (ret != is[9]) {
//				if (is[9] == 0)
//					dt.falsePositive++;
//				if (is[9] == 1)
//					dt.falseNegative++;
//
//			} else {
//				if (ret == 1) {
//					dt.truePositive++;
//				} else {
//					dt.trueNegative++;
//				}
//			}
//		}
		//
		// dt.accuracy = (1.0 * (dt.truePositive + dt.trueNegative))
		// / (dt.truePositive + dt.trueNegative + dt.falseNegative +
		// dt.falsePositive);
		// if ((dt.truePositive + dt.falsePositive) > 0)
		// dt.precision = (1.0 * dt.truePositive)
		// / (1.0 * (dt.truePositive + dt.falsePositive));
		// if ((dt.truePositive + dt.falseNegative) > 0)
		// dt.recall = (1.0 * dt.truePositive)
		// / (dt.truePositive + dt.falseNegative);
		// if ((dt.recall + dt.precision) > 0)
		// dt.fMeasure = (2.0 * dt.recall * dt.precision)
		// / (dt.recall + dt.precision);
		// double positiveAccuracy = (1.0 * dt.truePositive)
		// / (dt.truePositive + dt.falseNegative);
		// double negativeAccuracy = (1.0 * dt.trueNegative)
		// / (dt.trueNegative + dt.falsePositive);
		// double mul = positiveAccuracy * negativeAccuracy;
		// dt.gMean = Math.sqrt(mul);
		//
		// System.out.println(dt.accuracy + " " + dt.precision + " " + dt.recall
		// + " " + dt.gMean + " " + dt.fMeasure);
		// System.out.println(dt.calculateBestAttribute());
	}

}
